package com.nestorak.nazar.controller;

import com.nestorak.nazar.model.Shop;
import com.nestorak.nazar.model.Viddil;
import com.nestorak.nazar.view.View;

import java.util.Scanner;

public class Controller {
    Shop shop = new Shop();
    View view = new View();
    Viddil viddil = new Viddil();
    Scanner scanner = new Scanner(System.in);

    public void menuFurniture() {
        int productChoice = scanner.nextInt();
        if (productChoice == 1) {
            view.showProductsFromCheapToExpencive(viddil.getFromCheapToExpenciveFurniture());
        } else if (productChoice == 2) {
            view.showProductsFromExpenciveToCheap(viddil.getFromExpenciveToCheapFurniture());
        } else if (productChoice == 3) {
            System.out.println("Please specify a price range for sorting Furniture:"+ viddil);
            int min = scanner.nextInt();
            int max = scanner.nextInt();
            view.showProductsFromCheapToGiven(viddil.getFurnitureFromCheapToGiven(min, max));
        }else {
            shop.getWrongNumber();
            menuFurniture();
        }
    }

    public void menuWoodProduct() {
        int productChoice = scanner.nextInt();
        if (productChoice == 1) {
            view.showProductsFromCheapToExpencive(viddil.getFromCheapToExpenciveWood());
        } else if (productChoice == 2) {
            view.showProductsFromExpenciveToCheap(viddil.getFromExpenciveToCheapWood());
        } else if (productChoice == 3) {
            System.out.println("Please specify a price range for sorting WoodProducts:");
            int min = scanner.nextInt();
            int max = scanner.nextInt();
            view.showProductsFromCheapToGiven(viddil.getWoodFromCheapToGiven(min, max));
        }else {
            shop.getWrongNumber();
            menuWoodProduct();
        }
    }

    public void menuWoodTool() {
        int productChoice = scanner.nextInt();
        if (productChoice == 1) {
            view.showProductsFromCheapToExpencive(viddil.getFromCheapToExpenciveTool());
        } else if (productChoice == 2) {
            view.showProductsFromExpenciveToCheap(viddil.getFromExpenciveToCheapTool());
        } else if (productChoice == 3) {
            System.out.println("Please specify a price range for sorting Tools: ");
            int min = scanner.nextInt();
            int max = scanner.nextInt();
            view.showProductsFromCheapToGiven(viddil.getToolFromCheapToGiven(min, max));
        } else {
            shop.getWrongNumber();
            menuWoodTool();
        }
    }

    public void mainMenu() {
        view.showShopMenu(shop.getHello(), shop.getOurProducts());
        int menuChoice = scanner.nextInt();
        if (menuChoice == 1) {
            view.showProductFilter(shop.getProductFilter());
            menuFurniture();
        }else if (menuChoice == 2) {
            view.showProductFilter(shop.getProductFilter());
            menuWoodProduct();
        } else if (menuChoice == 3) {
            view.showProductFilter(shop.getProductFilter());
            menuWoodTool();
        } else {
            shop.getWrongNumber();
            mainMenu();
        }
    }
}
