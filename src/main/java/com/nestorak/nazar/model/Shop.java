package com.nestorak.nazar.model;

public class Shop {

    private String hello = "Please choose what interests you:";
    private String ourProducts = "3 viddil of products available for you: "+"\n"+
            "1 - Viddil of Furniture"+"\n"+
            "2 - Viddil of WoodProduct"+"\n"+
            "3 - Viddil of Tools"+"\n"+
            "Make your choice, please:"+"\n";
    private String productFilter = "Please choose what you wont to do:"+"\n"+
            "1 - view products at a price from cheap to more expensive"+"\n"+
            "2 - view products at a price from more expensive to cheap"+"\n"+
            "3 - view products in given price range";
    private String wrongNumber = "Wrong number. Mast be from 1 to 3:";

    public Shop() {
    }

    public Shop(String hello, String ourProducts, String productFilter) {
        this.hello = hello;
        this.ourProducts = ourProducts;
        this.productFilter = productFilter;
    }

    public String getHello() {
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }

    public String getOurProducts() {
        return ourProducts;
    }

    public String getWrongNumber() {
        return wrongNumber;
    }

    public void setOurProducts(String ourProducts) {
        this.ourProducts = ourProducts;
    }

    public String getProductFilter() {
        return productFilter;
    }

    public void setProductFilter(String productFilter) {
        this.productFilter = productFilter;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "hello='" + hello + '\'' +
                ", ourProducts='" + ourProducts + '\'' +
                ", productFilter='" + productFilter + '\'' +
                '}';
    }
}
