package com.nestorak.nazar.model;

import java.util.*;
import java.util.stream.Collectors;

public class Viddil {
    private String viddilTitle;
    private ArrayList<Product> productlist = new ArrayList<>();

    public ArrayList<Product> createProductFurniture() {
        Product furniture1 = new Product("Sofa", 2000);
        Product furniture2 = new Product("Sofa", 5000);
        Product furniture3 = new Product("Sofa", 10000);
        Product furniture4 = new Product("Table", 700);
        Product furniture5 = new Product("Table", 500);
        Product furniture6 = new Product("Table", 1000);
        productlist.add(furniture1);
        productlist.add(furniture2);
        productlist.add(furniture3);
        productlist.add(furniture4);
        productlist.add(furniture5);
        productlist.add(furniture6);
        return productlist;
    }

    public ArrayList<Product> createProductWood() {
        Product wood1 = new Product("Door", 2000);
        Product wood2 = new Product("Flor", 5000);
        Product wood3 = new Product("Flor", 10000);
        Product wood4 = new Product("Door", 400);
        Product wood5 = new Product("Door", 500);
        Product wood6 = new Product("Door", 500);
        productlist.add(wood1);
        productlist.add(wood2);
        productlist.add(wood3);
        productlist.add(wood4);
        productlist.add(wood5);
        productlist.add(wood6);
        return productlist;
    }

    public ArrayList<Product> createProductTool() {
        Product tool1 = new Product("Drill", 2000);
        Product tool2 = new Product("Drill", 5000);
        Product tool3 = new Product("Drill", 10000);
        Product tool4 = new Product("Hammer", 90);
        Product tool5 = new Product("Hammer", 150);
        Product tool6 = new Product("Hammer", 200);
        productlist.add(tool1);
        productlist.add(tool2);
        productlist.add(tool3);
        productlist.add(tool4);
        productlist.add(tool5);
        productlist.add(tool6);
        return productlist;
    }

    public List<Product> getFromCheapToExpenciveFurniture() {
        ArrayList<Product> furnitureList = createProductFurniture();
        List<Product> productList = furnitureList
                .stream()
                .sorted((o1, o2) -> o1.getPrice() - o2.getPrice())
                .collect(Collectors.toList());
        return productList;
    }

    public List<Product> getFromExpenciveToCheapFurniture() {
        ArrayList<Product> furnitureList = createProductFurniture();
        List<Product> productList = furnitureList
                .stream()
                .sorted((o1, o2) -> o2.getPrice() - o1.getPrice())
                .collect(Collectors.toList());
        return productList;
    }

    public List<Product> getFromCheapToExpenciveWood() {
        ArrayList<Product> furnitureList = createProductWood();
        List<Product> productList = furnitureList
                .stream()
                .sorted((o1, o2) -> o1.getPrice() - o2.getPrice())
                .collect(Collectors.toList());
        return productList;
    }

    public List<Product> getFromExpenciveToCheapWood() {
        ArrayList<Product> furnitureList = createProductWood();
        List<Product> productList = furnitureList
                .stream()
                .sorted((o1, o2) -> o2.getPrice() - o1.getPrice())
                .collect(Collectors.toList());
        return productList;
    }

    public List<Product> getFromCheapToExpenciveTool() {
        ArrayList<Product> furnitureList = createProductTool();
        List<Product> productList = furnitureList
                .stream()
                .sorted((o1, o2) -> o1.getPrice() - o2.getPrice())
                .collect(Collectors.toList());
        return productList;
    }

    public List<Product> getFromExpenciveToCheapTool() {
        ArrayList<Product> furnitureList = createProductTool();
        List<Product> productList = furnitureList
                .stream()
                .sorted((o1, o2) -> o2.getPrice() - o1.getPrice())
                .collect(Collectors.toList());
        return productList;
    }

    public List<Product> getFurnitureFromCheapToGiven(int choicePriceMin, int choicePriceMax) {
        List<Product> products = createProductFurniture()
                .stream()
                .filter(product -> product.getPrice() >= choicePriceMin && product.getPrice() <= choicePriceMax)
                .collect(Collectors.toList());
        return products;
    }

    public List<Product> getWoodFromCheapToGiven(int choicePriceMin, int choicePriceMax) {
        List<Product> products = createProductFurniture()
                .stream().filter(product -> product.getPrice() >= choicePriceMin && product.getPrice() <= choicePriceMax)
                .collect(Collectors.toList());
        return products;
    }

    public List<Product> getToolFromCheapToGiven(int choicePriceMin, int choicePriceMax) {
        List<Product> products = createProductFurniture()
                .stream()
                .filter(product -> product.getPrice() >= choicePriceMin && product.getPrice() <= choicePriceMax)
                .collect(Collectors.toList());
        return products;
    }

    public Viddil() {
    }

    public Viddil(String viddilTitle, ArrayList<Product> productlist) {
        this.viddilTitle = viddilTitle;
        this.productlist = productlist;
    }

    public String getViddilTitle() {
        return viddilTitle;
    }

    public void setViddilTitle(String viddilTitle) {
        this.viddilTitle = viddilTitle;
    }

    public ArrayList<Product> getProductlist() {
        return productlist;
    }

    public void setProductlist(ArrayList<Product> productlist) {
        this.productlist = productlist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Viddil)) return false;
        Viddil viddil = (Viddil) o;
        return Objects.equals(viddilTitle, viddil.viddilTitle) &&
                Objects.equals(productlist, viddil.productlist);
    }

    @Override
    public int hashCode() {

        return Objects.hash(viddilTitle, productlist);
    }

    @Override
    public String toString() {
        return "Viddil{" +
                "viddilTitle='" + viddilTitle + '\'' +
                ", productlist=" + productlist +
                '}';
    }
}
