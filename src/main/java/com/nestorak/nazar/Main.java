package com.nestorak.nazar;

import com.nestorak.nazar.controller.Controller;

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.mainMenu();
    }
}
