package com.nestorak.nazar.view;

import com.nestorak.nazar.model.Product;

import java.util.List;

public class View {

    public void showShopMenu(String hello, String ourProducts) {
        System.out.println(hello);
        System.out.println(ourProducts);
    }

    public void showProductFilter(String productFilter) {
        System.out.println(productFilter);
    }

    public void showProductsFromCheapToExpencive(List<Product> products) {
        products.forEach(System.out::println);
    }

    public void showProductsFromExpenciveToCheap(List<Product> products) {
        products.forEach(System.out::println);
    }

    public void showProductsFromCheapToGiven(List<Product> products) {
        products.forEach(System.out::println);
    }
}

